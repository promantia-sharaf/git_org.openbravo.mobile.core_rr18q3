/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.mobile.core.servercontroller;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.enterprise.Organization;

/**
 * Process that will be invoked by a remote server to check the availability of the current server
 * 
 * It just does a quick database check. If the remove server receives the OK response, it will
 * assume that the current server is available
 */
public class CheckServerAvailability extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    JSONObject response = new JSONObject();
    String status = null;
    try {
      // Do a quick database check to ensure it responds
      Organization organization = OBDal.getInstance().get(Organization.class, "0");
      status = organization != null ? "OK" : "NOK";
    } catch (Exception e) {
      status = "NOK";
    }
    response.put("serverStatus", status);
    return response;
  }
}
