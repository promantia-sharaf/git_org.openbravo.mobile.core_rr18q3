/*
 ************************************************************************************
 * Copyright (C) 2016-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.mobile.core.servercontroller;

import java.io.StringWriter;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.util.Check;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.mobile.core.process.JSONRowConverter;
import org.openbravo.mobile.core.utils.OBMOBCUtils;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.service.importprocess.ImportEntry;
import org.openbravo.service.importprocess.ImportEntryArchive;
import org.openbravo.service.importprocess.ImportEntryArchivePreProcessor;
import org.openbravo.service.importprocess.ImportEntryManager;
import org.openbravo.service.importprocess.ImportEntryPreProcessor;
import org.openbravo.service.importprocess.ImportProcessUtils;
import org.openbravo.service.json.JsonConstants;

/**
 * Supports creating import entries to easily replicate an action to another server. Also uses the
 * import entry archive table to check for and handle duplicate requests. Errors are stored in
 * import entries if needed.
 * 
 * Also supports pre and post processing actions/hooks.
 * 
 * For more information see this wiki page:
 * http://wiki.openbravo.com/wiki/Retail:Store_Server#Synchronized_Transactions
 * 
 * @author mtaal
 */
public abstract class MultiServerJSONProcess extends JSONProcessSimple {

  public static final String CALL_CENTRAL_PROP = "_tryCentralFromStore";
  public static final String CALL_ONLY_CENTRAL_PROP = "_onlyDoCentralFromStore";
  public static final String EXECUTE_IN_ONE_SERVER = "_executeInOneServer";
  public static final String RESULT_PROP = "_result";
  /**
   * @deprecated use {@link MobileServerRequestExecutor#SOURCE_PROP}
   */
  @Deprecated
  public static final String SOURCE_PROP = MobileServerRequestExecutor.SOURCE_PROP;
  /**
   * @deprecated use {@link MobileServerRequestExecutor#SOURCE_WEBPOS}
   */
  @Deprecated
  public static final String SOURCE_WEBPOS = MobileServerRequestExecutor.SOURCE_WEBPOS;
  /**
   * @deprecated use {@link MobileServerRequestExecutor#SOURCE_STORE}
   */
  @Deprecated
  public static final String SOURCE_STORE = MobileServerRequestExecutor.SOURCE_STORE;
  /**
   * @deprecated use {@link MobileServerRequestExecutor#SOURCE_CENTRAL}
   */
  @Deprecated
  public static final String SOURCE_CENTRAL = MobileServerRequestExecutor.SOURCE_CENTRAL;

  private static final Logger log = Logger.getLogger(MultiServerJSONProcess.class);

  private Entity entryEntity = ModelProvider.getInstance().getEntity(ImportEntry.ENTITY_NAME);
  private Entity archiveEntity = ModelProvider.getInstance().getEntity(
      ImportEntryArchive.ENTITY_NAME);

  @Inject
  @Any
  private Instance<ImportEntryArchivePreProcessor> archiveEntryPreProcessors;

  @Inject
  private ImportEntryManager importEntryManager;

  @Inject
  @Any
  private Instance<ImportEntryPreProcessor> entryPreProcessors;

  @Inject
  @Any
  private Instance<MultiServerJSONProcessHook> processHooks;

  @Override
  public JSONObject exec(JSONObject jsonSent) {
    // logTime is used to relate log statements in begin and end to eachother
    final long logTime = System.currentTimeMillis();
    Throwable thrownError = null;

    if (log.isDebugEnabled()) {
      log.debug("Pre-Handling of json (" + logTime + ") input " + jsonSent);
    }

    // do a pre-check if the message was already handled somehow
    OBContext.setAdminMode(true);
    try {
      final ImportEntryArchive archiveEntry = getImportEntryArchive(jsonSent);
      if (archiveEntry != null) {
        // entry present, so already being handled, but check different states
        final String importStatus = archiveEntry.getImportStatus();
        if ("Error".equals(importStatus)) {
          throw new OBException(archiveEntry.getErrorinfo());
        } else if ("Processed".equals(importStatus)) {
          // in other cases, just return success
          return createSuccessResponse(jsonSent, new JSONObject());
        } else {
          // still being processed, show message
          final JSONObject result = createSuccessResponse(jsonSent, new JSONObject());
          result.put("showMessage",
              OBMessageUtils.getI18NMessage("OBMOBC_DataIsBeingProcessed", null));
          return result;
        }
      } else if (jsonSent.has("messageId") && getImportEntryId() == null
          && !doesImportEntryExist(jsonSent.getString("messageId"))) {
        // only create an archive entry if there is no import entry
        createArchiveEntry(jsonSent.getString("messageId"), jsonSent);
        OBDal.getInstance().commitAndClose();
      } else if (jsonSent.has("messageId") && getImportEntryId() == null
          && doesImportEntryExist(jsonSent.getString("messageId"))) {
        log.warn("The message was already processed, there is an import entry - archive for it, "
            + "not processing this duplicate message, message content: " + jsonSent);
        return createSuccessResponse(jsonSent, new JSONObject());
      }
    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }

    // returning must be done using the returnResult at the end of the try-finally
    // don't return in the middle as extra checking is done at the end of the try-finally
    JSONObject returnResult = null;
    boolean errorOccurred = true;
    executeProcessHooks(jsonSent);
    String messageId = null;
    try {
      // make sure the json has a message id so duplicates can be detected
      if (!jsonSent.has("messageId")) {
        jsonSent.put("messageId", SequenceIdData.getUUID());
      }
      messageId = jsonSent.getString("messageId");

      // check if allowed to execute, if this is a store server
      if (MobileServerController.getInstance().isThisAStoreServer()
          && executeOnlyInCentral(jsonSent)) {
        returnResult = OBMOBCUtils.createSimpleErrorJson("OBMOBC_MsgCentralServerNotAvailable");
      } else {
        // execute the action
        returnResult = doExecute(jsonSent);
      }

      if (log.isDebugEnabled()) {
        log.debug("Result (" + logTime + "): " + returnResult);
      }

      // last checks, return is done at the end of the method
      Check.isNotNull(returnResult, "Return result may not be null");
      errorOccurred = isErrorJson(returnResult);

      // set correct import entry status
      if (!errorOccurred) {
        OBContext.setAdminMode();
        try {
          if (getImportEntryId() != null
              && !importEntryManager.isImportEntryError(getImportEntryId())) {
            importEntryManager.setImportEntryProcessed(getImportEntryId());
          }

          if (getImportEntryId() == null) {
            updateArchiveStatus(messageId, "Processed");
          }

          OBDal.getInstance().commitAndClose();
        } finally {
          OBContext.restorePreviousMode();
        }

        // kick the import entry thread to process right away
        importEntryManager.notifyNewImportEntryCreated();
      }

    } catch (Throwable t) {
      thrownError = t;
      log.error(t.getMessage() + " - " + (jsonSent != null ? jsonSent.toString() : "NULL"), t);

      final StringWriter sw = new StringWriter();
      JSONRowConverter.addJSONExceptionFields(sw, t);
      try {
        return new JSONObject("{" + sw.toString() + "}");
      } catch (JSONException e) {
        throw new OBException(e);
      }
    } finally {
      if (thrownError != null || errorOccurred) {
        try {
          OBDal.getInstance().rollbackAndClose();
        } catch (Throwable ignore) {
          // don't hide the other error
        }

        // set the import entry to error
        if (getImportEntryId() != null) {
          try {

            if (thrownError == null) {
              // error json
              thrownError = new Exception(returnResult != null ? returnResult.toString()
                  : "ERROR No returnResult");
            }

            importEntryManager.setImportEntryErrorIndependent(getImportEntryId(), thrownError);
          } catch (Throwable ignore) {
            // don't hide the other error
          }
        } else {
          if (thrownError == null) {
            thrownError = new Exception(returnResult != null ? returnResult.toString()
                : "ERROR No returnResult");
          }
          setImportEntryArchiveErrorIndependent(messageId, thrownError);
        }
      }
      // is not really needed as this instance should not be re-used, but to be safe
      setImportEntryId(null);
    }

    for (MultiServerJSONProcessHook processor : processHooks) {
      processor.doAction(returnResult);
    }

    // we get here if no exception was thrown in the inner code above
    return returnResult;
  }

  private ImportEntryArchive getImportEntryArchive(JSONObject jsonSent) {
    try {
      if (!jsonSent.has("messageId")) {
        return null;
      }
      return OBDal.getInstance().get(ImportEntryArchive.class, jsonSent.get("messageId"));
    } catch (JSONException e) {
      throw new OBException(e);
    }
  }

  /**
   * Return the data type string used when creating the import entry
   */
  protected String getImportEntryDataType() {
    throw new OBException(
        "MultiServerJSONProcess doesn't implement the getImportEntryDataType() method by default, subclasses need to implement it if they are designed to use the Import Entry infrastructure.");
  }

  /**
   * This method should do the real action, and return the json response.
   * 
   * Any error situation should be handled by throwing an exception. This exception is handled in
   * this class and returned to the caller of this class.
   */
  protected abstract JSONObject execute(JSONObject json);

  private JSONObject doExecute(JSONObject jsonSent) throws Exception {

    final JSONObject result = executeLocal(jsonSent);

    if (!isErrorJson(result) && !executeInOneServer(jsonSent)) {
      createImportEntry(jsonSent.getString("messageId"), jsonSent, result, null);
      // is set to processed to in finally block of main loop
    }
    return result;
  }

  protected JSONObject executeLocal(JSONObject jsonSent) throws Exception {

    final JSONObject result = execute(jsonSent);

    // error was returned here
    if (isErrorJson(result)) {
      return result;
    }

    return createSuccessResponse(jsonSent, result);
  }

  /**
   * Controls if when the request is being handled in the store it will return an error json.
   * Default is false.
   */
  protected boolean executeOnlyInCentral(JSONObject json) throws JSONException {
    return json.has(CALL_ONLY_CENTRAL_PROP) && json.getBoolean(CALL_ONLY_CENTRAL_PROP);
  }

  /**
   * Execute the action in one server at most, assume that any transactions are replicated through
   * other means than import entry.
   */
  protected boolean executeInOneServer(JSONObject json) throws JSONException {
    return json.has(EXECUTE_IN_ONE_SERVER) && json.getBoolean(EXECUTE_IN_ONE_SERVER);
  }

  /**
   * Controls if the store should first call central and then create a local import entry for local
   * transactions.
   * 
   * This method is not used anymore.
   * 
   * @deprecated
   */
  @Deprecated
  protected boolean executeFirstInCentral(JSONObject json) throws JSONException {
    log.warn("The executeFirstInCentral method is not used anymore");
    return json.has(CALL_CENTRAL_PROP) && json.getBoolean(CALL_CENTRAL_PROP);
  }

  /**
   * Creates the import entry by combining the result json and the json which was sent in.
   * 
   * Organization in which the import entry will be created can be passed in. This to allow
   * distributing the import entry to other stores.
   * 
   * If null is passed in for the organization then as a default the system will use the current
   * organization of the user.
   * 
   * Override this method and call the super implementation with a different organization if this is
   * functionally relevant.
   */
  protected void createImportEntry(String messageId, JSONObject sentIn, JSONObject processResult,
      Organization organization) throws JSONException {
    // don't create import entry in error
    if (isErrorJson(processResult)) {
      log.error("Error result not creating import entry " + processResult.toString());
      return;
    }
    // the finally block in the main loop uses this to set status processed
    // There is one case where the entry should not be set to processed, that's done by resetting
    // the import entry id to null setImportEntryId(null) see executeFromWebPOSInStore
    setImportEntryId(messageId);

    final JSONObject result = new JSONObject(sentIn.toString());
    result.put(RESULT_PROP, processResult);

    OBContext.setAdminMode(true);
    try {
      if (doesImportEntryExist(messageId)) {
        return;
      }

      final ImportEntry importEntry = localCreateImportEntry(messageId, result, organization);

      OBDal.getInstance().save(importEntry);
    } finally {
      OBContext.restorePreviousMode();
    }
    // note: set to processed is done in a finally block later
  }

  private ImportEntry localCreateImportEntry(String messageId, JSONObject json,
      Organization organization) {

    ImportEntry importEntry = OBProvider.getInstance().get(ImportEntry.class);
    importEntry.setId(messageId);
    importEntry.setRole(OBDal.getInstance().getProxy(Role.class,
        OBContext.getOBContext().getRole().getId()));
    importEntry.setNewOBObject(true);
    importEntry.setImportStatus("Initial");
    importEntry.setImported(null);
    importEntry.setTypeofdata(getImportEntryDataType());
    importEntry.setJsonInfo(json.toString());
    if (organization != null) {
      importEntry.setOrganization(organization);
    }

    for (ImportEntryPreProcessor processor : entryPreProcessors) {
      processor.beforeCreate(importEntry);
    }
    return importEntry;
  }

  private boolean doesImportEntryExist(String id) {
    // check if it is not there already or already archived
    {
      final Query qry = SessionHandler.getInstance().getSession()
          .createQuery("select 1 from " + ImportEntry.ENTITY_NAME + " where id=:id");
      qry.setParameter("id", id);
      qry.setMaxResults(1);
      if (qry.list().size() > 0) {
        return true;
      }
    }
    {
      final Query qry = SessionHandler.getInstance().getSession()
          .createQuery("select 1 from " + ImportEntryArchive.ENTITY_NAME + " where id=:id");
      qry.setParameter("id", id);
      qry.setMaxResults(1);
      if (qry.list().size() > 0) {
        return true;
      }
    }
    return false;
  }

  /**
   * Update status of import entry archive
   */
  protected void updateArchiveStatus(String id, String status) {
    final ImportEntryArchive archiveEntry = OBDal.getInstance().get(ImportEntryArchive.class, id);
    if (archiveEntry != null) {
      archiveEntry.setImportStatus(status);
    }
  }

  private void setImportEntryArchiveErrorIndependent(String id, Throwable t) {
    OBDal.getInstance().rollbackAndClose();
    final OBContext prevOBContext = OBContext.getOBContext();
    OBContext.setOBContext("0", "0", "0", "0");
    try {
      // do not do org/client check as the error can be related to org/client access
      // so prevent this check to be done to even be able to save org/client access
      // exceptions
      OBContext.setAdminMode(false);
      ImportEntryArchive importEntry = OBDal.getInstance().get(ImportEntryArchive.class, id);
      if (importEntry != null) {
        importEntry.setImportStatus("Error");
        importEntry.setErrorinfo(ImportProcessUtils.getErrorMessage(t));
        OBDal.getInstance().save(importEntry);
        OBDal.getInstance().commitAndClose();
      }
    } catch (Throwable throwable) {
      try {
        OBDal.getInstance().rollbackAndClose();
      } catch (Throwable ignored) {
      }
      throw new OBException(throwable);
    } finally {
      OBContext.restorePreviousMode();
      OBContext.setOBContext(prevOBContext);
    }
  }

  /**
   * Create an archive entry to log what has been done.
   */
  protected void createArchiveEntry(String id, JSONObject json) throws JSONException {
    if (doesImportEntryExist(id)) {
      return;
    }

    OBContext.setAdminMode(true);
    try {
      // note import entry is only created to make it easier to create an archive
      // It should only be created and not saved as then it would be replicated
      // to other servers
      final ImportEntry importEntry = localCreateImportEntry(id, json, null);
      // create the archive
      final ImportEntryArchive archiveEntry = OBProvider.getInstance()
          .get(ImportEntryArchive.class);
      // copy properties with the same name
      for (Property sourceProperty : entryEntity.getProperties()) {
        // ignore these ones
        if (sourceProperty.isOneToMany() || !archiveEntity.hasProperty(sourceProperty.getName())) {
          continue;
        }
        Property targetProperty = archiveEntity.getProperty(sourceProperty.getName());
        // should be the same type
        if (targetProperty.getDomainType().getClass() != sourceProperty.getDomainType().getClass()) {
          continue;
        }
        try {
          archiveEntry
              .set(targetProperty.getName(), importEntry.getValue(sourceProperty.getName()));
        } catch (Exception e) {
          throw new OBException(e);
        }
      }

      for (ImportEntryArchivePreProcessor processor : archiveEntryPreProcessors) {
        processor.beforeArchive(importEntry, archiveEntry);
      }

      // as the id is also copied set it explicitly to new
      archiveEntry.setNewOBObject(true);
      OBDal.getInstance().save(archiveEntry);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private JSONObject createSuccessResponse(JSONObject jsonSent, JSONObject result) {
    try {
      final JSONObject response = new JSONObject();
      // put the message id and other info also in the result
      response.put("messageId", jsonSent.getString("messageId"));
      if (jsonSent.has("posTerminal")) {
        response.put("posTerminal", jsonSent.getString("posTerminal"));
      }
      if (!response.has(JsonConstants.RESPONSE_STATUS)) {
        response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
      }
      response.put(JsonConstants.RESPONSE_DATA, result);
      return response;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  protected boolean isErrorJson(JSONObject checkResult) throws JSONException {
    return checkResult.has(JsonConstants.RESPONSE_STATUS)
        && checkResult.get(JsonConstants.RESPONSE_STATUS).equals(
            JsonConstants.RPCREQUEST_STATUS_FAILURE);
  }

  protected boolean bypassSecurity() {
    return true;
  }

  protected boolean bypassPreferenceCheck() {
    return true;
  }

  public static abstract class MultiServerJSONProcessHook {
    public abstract void doAction(JSONObject json);

    public void preProcess(MultiServerJSONProcess process, JSONObject jsonsent) {
    }
  }

  private void executeProcessHooks(JSONObject jsonsent) {
    for (MultiServerJSONProcessHook hook : WeldUtils.getInstances(MultiServerJSONProcessHook.class)) {
      hook.preProcess(this, jsonsent);
    }
  }

}
