/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.mobile.core.polling;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.mobile.core.process.JSONProcessSimple;

/**
 * Abstract class that must be implemented by polling handlers.
 *
 * A Polling handler will receive the request based on the entity and application. It will return a
 * JSONObject with details about the process If the response is an error -> isError and errorMsg
 * property must be set
 */

public abstract class MobileCoreAbstractPollingHandler extends JSONProcessSimple {
  protected abstract JSONObject processPollingRequest(JSONObject request);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    return this.processPollingRequest(jsonsent);
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  @Override
  protected boolean bypassSecurity() {
    return true;
  }

}
