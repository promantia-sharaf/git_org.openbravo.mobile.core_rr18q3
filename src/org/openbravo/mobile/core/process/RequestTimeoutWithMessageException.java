/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.mobile.core.process;

import org.openbravo.base.exception.OBException;

/**
 * This exception is thrown when a request specifies a timeout, and this timeout is reached before
 * the request was able to finish
 *
 * This exception is used to catch a exception from classes where only have few info, and to be
 * throw again and be catch later adding the rest of the info for the log
 */
public class RequestTimeoutWithMessageException extends OBException {

  private static final long serialVersionUID = 1L;

  public RequestTimeoutWithMessageException(String message) {
    super(message);
  }

}
