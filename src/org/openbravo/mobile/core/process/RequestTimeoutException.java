package org.openbravo.mobile.core.process;

import org.openbravo.base.exception.OBException;

/**
 * This exception is thrown when a request specifies a timeout, and this timeout is reached before
 * the request was able to finish
 */
public class RequestTimeoutException extends OBException {

  private static final long serialVersionUID = 1L;

  public RequestTimeoutException() {
    super();
  }

  public RequestTimeoutException(String message) {
    super(message);
  }

}
