/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.mobile.core;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.mobile.core.process.DataSynchronizationImportProcess;
import org.openbravo.mobile.core.process.DataSynchronizationProcess;
import org.openbravo.mobile.core.process.DataSynchronizationProcess.DataSynchronization;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.json.JsonConstants;

@DataSynchronization(entity = "OBMOBC_TerminalLog")
public class TerminalLogLoader extends DataSynchronizationProcess implements
    DataSynchronizationImportProcess {

  private static final Logger log = Logger.getLogger(TerminalLogLoader.class);

  protected String getImportQualifier() {
    return "OBMOBC_TerminalLog";
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  public JSONObject saveRecord(JSONObject jsonEvents) throws Exception {

    try {

      // since we do a rollback in case of error, if using syncronized mode we ensure we don't
      // rollback other models.
      OBDal.getInstance().commitAndClose();

      JSONArray events = new JSONArray(jsonEvents.getString("events"));
      String deviceid = jsonEvents.getString("terminal");
      String cache_session_id = jsonEvents.getString("cacheSessionId");

      String processName;
      JSONObject processInfo;
      String processId;
      String elementId;

      String tstamp;
      String context;
      String isonline;
      String loglevel = "";
      String msg = "";
      String ad_org_id;
      String ad_client_id;
      String created;
      String createdby;
      String updated;
      String updatedby;

      ConnectionProvider cp = new DalConnectionProvider(false);

      ad_client_id = OBContext.getOBContext().getCurrentClient().getId();
      ad_org_id = OBContext.getOBContext().getCurrentOrganization().getId();
      createdby = OBContext.getOBContext().getUser().getId();

      final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

      for (int i = 0; i < events.length(); i++) {

        JSONObject event = events.getJSONObject(i);

        Timestamp timestamp = new Timestamp(Long.parseLong(event.getString("d")));

        created = dateFormatter.format(new Date(timestamp.getTime()));
        updated = created;
        updatedby = createdby;
        tstamp = String.valueOf(timestamp.getTime());
        context = event.getString("c");
        isonline = String.valueOf(event.getString("o").equals("true") ? "Y" : "N");

        if (event.has("e")) {
          elementId = "    -    (" + event.getString("e") + ")";
        } else {
          elementId = "";
        }

        switch (event.getString("t")) {
        case "k":
          loglevel = "UserAction";
          msg = "Typed: " + event.getString("k") + elementId;
          break;
        case "b":
          loglevel = "UserAction";
          msg = "Clicked on '" + event.getString("b") + "'" + elementId;
          break;
        case "bk":
          loglevel = "UserAction";
          msg = "Clicked on '" + event.getString("b") + "'" + elementId;
          break;
        case "s":
          loglevel = "Process";
          processName = event.getString("v");
          processInfo = new JSONObject(event.getString("i"));
          processId = processInfo.getString("id");
          msg = "Process:  " + processName + " - started      -      (" + processId + ")";
          break;
        case "f":
          loglevel = "Process";
          processName = event.getString("v");
          processInfo = new JSONObject(event.getString("i"));
          processId = processInfo.getString("id");
          msg = "Process:  " + processName + " - finished    -      (" + processId + ")";
          break;
        default:
          log.warn("Event " + event.getString("t") + " no supported");
        }

        UserEventLoaderQueriesData.insertUserEventLog(cp, //
            ad_client_id, //
            ad_org_id, //
            created, //
            createdby, //
            updated, //
            updatedby, //
            tstamp, //
            deviceid, //
            context, //
            msg, //
            loglevel, //
            cache_session_id, //
            isonline //
            );

      }

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log.error("Could not save UserEvent log with info: " + jsonEvents.toString());
    }
    return successMessage();
  }

  private JSONObject successMessage() throws Exception {
    final JSONObject jsonResponse = new JSONObject();

    jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    jsonResponse.put("result", "0");
    return jsonResponse;
  }

  @Override
  protected String getProperty() {
    return "OBMOBC_TerminalLog";
  }

  @Override
  public String getAppName() {
    return "Mobile Core";
  }

}