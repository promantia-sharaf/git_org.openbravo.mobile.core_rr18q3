/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.mobile.core.offline;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.openbravo.mobile.core.process.WebServiceAbstractServlet;

/**
 *  This class will fetch Service Worker file, take the content and create
 *  a response with that content and a Service-Worker-Allowed header to make
 *  Service Worker be able to mark to scope as root.
 *  @author   Javier Rodriguez Regueiro
 *  @since    2018-05-14
 */
public class ServiceWorkerManifestServlet extends WebServiceAbstractServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger log = Logger.getLogger(ServiceWorkerManifestServlet.class);

  private String fileUrl;

  /**
   * This method will execute when the server is initialized.
   * After to take the real path of Service Worker file, thi servlet
   * will call WebServiceAbstractServlet init.
   */
  @Override
  public void init(ServletConfig config) throws ServletException {
    fileUrl = config.getServletContext()
        .getRealPath("web/org.openbravo.mobile.core/source/offline/ob-sources-sw.js");
    super.init(config);
  }

  /**
   * This method will execute when a get request is received.
   * The method try to take the Service Worker file content and write it
   * in a String. If this is OK, writeResult will be called
   */
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {

    String ServiceWorkerContent = "";
    try {
      ServiceWorkerContent = new String(Files.readAllBytes(Paths.get(fileUrl)));
    } catch (Exception e) {
      log.error("An error happened while reading the service worker source code", e);
    }
    writeResult(response, ServiceWorkerContent);
  }
  
  /**
   * This method write into response to send when a get request is received.
   */
  protected void writeResult(HttpServletResponse response, String result) throws IOException {
    response.setHeader("Service-Worker-Allowed", "/");
    response.setContentType("application/javascript");

    final Writer w = response.getWriter();
    w.write(result);
    w.close();
  }

}
