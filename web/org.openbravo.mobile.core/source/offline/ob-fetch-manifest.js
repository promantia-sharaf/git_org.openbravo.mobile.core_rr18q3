/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global fetch*/

OB.UTIL.fetchApplicationSources = function(
  successNoRefreshCallback,
  errorCallback
) {
  var setManifest = false;
  var manifest;
  //make sure that Service Workers are supported.
  var url =
    '../../org.openbravo.mobile.core/OBPOS_Main/AppCacheManifest?_appName=WebPOS';
  var oldManifest;

  function send_message_to_sw() {
    var appData = {};
    appData.manifest = manifest;
    appData.appName = OB.MobileApp.model.get('appName');
    navigator.serviceWorker.controller.postMessage(appData);
  }

  fetch(url)
    .then(function(response) {
      if (response.status !== 200) {
        OB.error(
          'Error fetching the Manifest. Status Code: ' + response.status
        );
        return;
      }

      var responseClone = response.clone();

      // Examine the text in the response
      return responseClone.text().then(function(text) {
        manifest = text;
        oldManifest = OB.UTIL.localStorage.getItem('manifest');
        if (oldManifest === null) {
          OB.info('[Manifest] Dont have manifest yet');
          setManifest = true;
        } else {
          if (manifest !== oldManifest) {
            OB.info('[Manifest] Taking the meanifest new version');
            setManifest = true;
          } else {
            OB.info('[Manifest] Manifest is still up to date');
            successNoRefreshCallback();
          }
        }
      });
    })
    .then(function() {
      if (navigator.serviceWorker) {
        return navigator.serviceWorker
          .register(
            '../../org.openbravo.mobile.core.service.ServiceWorkerManifestServlet',
            {
              scope: '/'
            }
          )
          .then(function(registration) {
            // Registration was successful
            OB.info(
              'ServiceWorker registration successful with scope: ',
              registration.scope
            );
          })
          ['catch'](function(err) {
            // registration failed :(
            OB.error('ServiceWorker registration failed: ', err);
          });
      }
    })
    .then(function() {
      if (
        navigator.serviceWorker &&
        navigator.serviceWorker.controller &&
        navigator.serviceWorker.controller.state === 'activated' &&
        setManifest === true
      ) {
        send_message_to_sw();
      }
    })
    ['catch'](function() {
      if (OB.UTIL.isNullOrUndefined(OB.UTIL.localStorage.getItem('manifest'))) {
        errorCallback();
      } else {
        OB.info(
          "Manifest couldn't be retrieved, but we have valid sources, so offline mode will be enabled."
        );
        successNoRefreshCallback();
      }
    });

  if (navigator.serviceWorker) {
    navigator.serviceWorker.ready.then(function() {
      // Let's see if you have a subscription already
      // return serviceWorkerRegistration.pushManager.getSubscription();
      navigator.serviceWorker.addEventListener('controllerchange', function(e) {
        if (setManifest === true) {
          send_message_to_sw();
        }
      });
    });

    var failResource = [];
    navigator.serviceWorker.addEventListener('message', function(e) {
      switch (e.data.status) {
        case 'OBLog':
          var typeLog = e.data.typeLog;
          if (typeLog === 'info') {
            OB.info(e.data.message);
          }
          if (typeLog === 'error') {
            OB.error(e.data.message);
          }
          if (typeLog === 'warn') {
            OB.warn(e.data.message);
          }
          break;
        case 'beginning':
          OB.info('[Message in index] Beginning to fetch');
          OB.MobileApp.model.set('isLoggingIn', true);
          OB.UTIL.showLoading(true, null, 'OBPOS_LoadingWebPOSApplication');
          OB.UTIL.currentLoadingStep = 0;
          OB.UTIL.loadingSteps = e.data.steps;
          break;
        case 'success':
          OB.info('[Message in index] Success: ' + e.data.path);
          OB.UTIL.completeLoadingStep();
          OB.UTIL.showLoadingMessage(e.data.path);
          break;
        case 'fail':
          OB.info('[Message in index] Fail: ' + e.data.path);
          failResource.push(e.data.path);
          break;
        case 'end':
          OB.MobileApp.model.set('isLoggingIn', false);
          if (failResource.length === 0) {
            OB.info('[Everything is catched well]');
            OB.UTIL.localStorage.setItem('manifest', manifest);
            if (oldManifest !== null) {
              window.location.reload();
            } else {
              OB.UTIL.showLoading(false);
              successNoRefreshCallback();
            }
          } else {
            OB.UTIL.showLoading(false);
            errorCallback();
          }
          break;
      }
    });
  }
};
