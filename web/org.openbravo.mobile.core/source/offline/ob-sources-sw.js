/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global console, clients, fetch, caches, self, Promise*/

//Variables that will be used.
var cacheName;
var cacheFiles = [
  'org.openbravo.mobile.core.service.ServiceWorkerManifestServlet'
];
var lineToAdd;

var receivedManifest;

function sendOBLog(typeLog, message) {
  return this.clients
    .matchAll()
    .then(function(clients) {
      var messageInfo = {
        status: 'OBLog',
        typeLog: typeLog,
        message: message
      };
      clients.forEach(function(client) {
        client.postMessage(messageInfo);
      });
    })
    ['catch'](function(error) {
      console.error('[Servcice Worker] Failed to send log');
    });
}

//  Function fetchManifest(manifest)
//  Parameters: manifest
//  Returns: cacheFiles
//  Description: This function take a manifest and parser
//              it to save, in an array, only the URL of
//              the resource that we just need.

function fetchManifest(manifest) {
  sendOBLog('info', '[ServiceWorker] Cleaning manifest');
  return this.clients
    .matchAll()
    .then(function(clients) {
      clients.forEach(function(client) {
        cacheFiles[cacheFiles.length] = client.url;
      });
    })
    .then(function() {
      var lines = manifest.split('\n');
      var indexValue;
      var line;
      for (line = 0; line < lines.length; line++) {
        if (lines[line] !== '') {
          if (
            lines[line][0] !== '\n' &&
            lines[line][0] !== 'C' &&
            lines[line][0] !== 'N' &&
            lines[line][0] !== '#' &&
            lines[line][0] !== '*'
          ) {
            lineToAdd = lines[line];
            lineToAdd = lineToAdd.replace('../../', '');
            cacheFiles[cacheFiles.length] = lineToAdd;
          }
        }
      }
      sendOBLog('info', '[ServiceWorker] Manifest cleaned');
    });
}

//  Function loadResource(resourceToCache, path)
//  Parameters: resourceToCache, path
//  Returns: The Promise of a fetch
//  Description: This function try to fetch a resource with a specific path,
//              create a object which contains the fetch response and the path
//              and then, push it into the array resourceToCache

function loadResource(resourceToCache, path) {
  function sendErrorMessage(path) {
    clients.matchAll().then(function(clients) {
      var messageInfo = {
        status: 'fail',
        path: path
      };
      clients.forEach(function(client) {
        client.postMessage(messageInfo);
      });
    });
  }
  return fetch(path)
    .then(function(response) {
      if (!response.ok) {
        sendErrorMessage(path);
        return;
      }
      var messageLoadResource = 'Resource loaded: ' + path;
      sendOBLog('info', '[ServiceWorker] Manifest cleaned');
      this.clients.matchAll().then(function(clients) {
        var separator = null;
        var indexElement = path.length;
        while (separator !== '/' && indexElement !== 0) {
          separator = path.substring(indexElement - 1, indexElement);
          indexElement = indexElement - 1;
        }
        path = path.substring(indexElement);

        var messageInfo = {
          status: 'success',
          path: path
        };
        clients.forEach(function(client) {
          client.postMessage(messageInfo);
        });
      });
      if (response.redirected) {
        return;
      }
      var responseClone = response.clone();
      var value = {
        response: responseClone,
        path: path
      };
      resourceToCache.push(value);
      return responseClone;
    })
    ['catch'](function(error) {
      sendErrorMessage(path);
    });
}

//  Function cacheResource(response, path)
//  Parameters: response, path
//  Returns: Promise of open then cache
//  Description: Open the service worker cache, then try to put the response
//              and their own path in the cache.

function cacheResource(response, path) {
  return caches
    .open(cacheName)
    .then(function(cache) {
      var message = '[loadResource] ' + path;
      sendOBLog('info', message);
      cache.put(path, response);
    })
    ['catch'](function(error) {
      var message = '[Error loadResource] ' + path;
      sendOBLog('error', message);
    });
}

//  Listener to install
//  Description: When the Service Worker has been installed,
//              the Service Worker will call the function to
//              stop waiting no be activate.
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Installed');
  self.skipWaiting();
});

//  Listener to Activate
//  Description: When the Service Worker has been activate,
//              the Service Worker will call the function to
//              allow the client to control it.
self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Actived');
  e.waitUntil(self.clients.claim());
});

//  Listener to fetch
//  Description: When the Service Worker catch the event fetch
//              it will try to look for it in their cache. If
//              it is there, the Service Worker will answer with
//              the response. In the other case, it will just answer
//              with a fail response.
self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(response) {
      if (response) {
        return response;
      }

      //It is necessary to clone the request because it is a stream.
      //stream is a single consume item, so we need to have a copy
      //for it, one to cache it and the other to the browser to fetch.
      var fetchRequest = e.request.clone();

      return fetch(fetchRequest)
        .then(function(response) {
          return response;
        })
        ['catch'](function() {
          //No need to report failed requests here
        });
    })
  );
});

//  Listener to message
//  Description: When the Service Worker catch a message event,
//              it take the message data that the index sent.
//              The Service Worker calls fetchManifest function
//              to clean the manifest and try to keep it in cache
//              using loadResource function and cacheResource function.
self.addEventListener('message', function(e) {
  receivedManifest = e.data.manifest;
  cacheName = e.data.appName;
  caches['delete'](cacheName).then(function() {
    var messageFetch =
      '[Service Worker] Fetching manifest: ' + receivedManifest;
    sendOBLog('info', messageFetch);
    fetchManifest(receivedManifest)
      .then(function() {
        this.clients
          .matchAll()
          .then(function(clients) {
            var messageInfo = {
              status: 'beginning',
              steps: cacheFiles.length
            };
            clients.forEach(function(client) {
              client.postMessage(messageInfo);
            });
          })
          .then(function() {
            var loadingPromises = [];
            var allPromises = [];
            var resourceToCache = [];
            sendOBLog('info', '[ServiceWorker] Caching cacheFiles');
            cacheFiles.forEach(function(resourcePath) {
              var loadedResource = loadResource(resourceToCache, resourcePath);
              if (loadedResource) {
                loadingPromises.push(loadedResource);
              }
            });
            return Promise.all(loadingPromises).then(function() {
              this.clients.matchAll().then(function(clients) {
                var messageInfo = {
                  status: 'end',
                  path: 'end'
                };
                clients.forEach(function(client) {
                  client.postMessage(messageInfo);
                });
              });
              sendOBLog('info', '[Service Worker] Finished loading promises');
              resourceToCache.forEach(function(resorce) {
                allPromises.push(cacheResource(resorce.response, resorce.path));
              });
              return Promise.all(allPromises)
                .then(function() {
                  sendOBLog(
                    'info',
                    '[Service Worker] Finished caching resources'
                  );
                })
                ['catch'](function() {
                  sendOBLog(
                    'error',
                    '[Service Worker Error] Error finishing caching resources'
                  );
                });
            });
          });
      })
      ['catch'](function() {
        sendOBLog('error', '[Service Worker Error] Error fetching manifest');
      });
  });
});
