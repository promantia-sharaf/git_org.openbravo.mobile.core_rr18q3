/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 *
 * Author MDJ
 *
 */

/*global OB, _, enyo */
/**
 * This class extends DOM nodes to log action, avoid wrong users actions...
 *
 */

(function() {
  function avoidDoubleClick(event, elmt) {
    //if the owner or the component defines avoidDoubleClick=false explicitly we will skip. If it is not defined --> avoidDoubleClick !== false
    if (
      enyo.$[elmt.id] &&
      enyo.$[elmt.id].tap &&
      enyo.$[elmt.id].avoidDoubleClick !== false &&
      enyo.$[elmt.id].container.avoidDoubleClick !== false
    ) {
      enyo.$[elmt.id].origTap = enyo.$[elmt.id].tap;
      enyo.$[elmt.id].tap = _.debounce(enyo.$[elmt.id].origTap, 200, true);
    }
  }

  function mouseDown(event) {
    var elmt =
      _.find(event.path, function(el) {
        return el.tagName === 'BUTTON';
      }) || event.target;
    OB.UTIL.TerminalLog.addButtonClick(elmt);
    avoidDoubleClick(event, elmt);
  }
  var debounceMouseDown = _.debounce(mouseDown, 1);
  document.addEventListener('DOMNodeInserted', function(nodeEvent) {
    if (nodeEvent && nodeEvent.srcElement) {
      nodeEvent.srcElement.onmousedown = debounceMouseDown;
    }
  });
  document.onkeydown = function(k) {
    if (k.key) {
      OB.UTIL.TerminalLog.addKeyPress(k);
    }
  };
})();
