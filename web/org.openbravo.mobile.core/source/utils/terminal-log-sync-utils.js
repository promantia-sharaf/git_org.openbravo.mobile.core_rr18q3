/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, console, _, Backbone*/

(function() {
  OB.UTIL = window.OB.UTIL || {};

  OB.UTIL.activateTerminalLogSync = function() {
    if (!OB.UTIL.terminalLogSyncIntervalId) {
      var terminalLogIntervalInSecs;
      if (OB.MobileApp.model.get('permissions')) {
        terminalLogIntervalInSecs = OB.MobileApp.model.get('permissions')
          .OBMOBC_TerminalLogSyncInterval;
      }
      if (!terminalLogIntervalInSecs) {
        // default value for OBMOBC_TerminalLogSyncInterval
        terminalLogIntervalInSecs = 30;
      }
      OB.UTIL.terminalLogSyncIntervalId = setInterval(function() {
        OB.UTIL.TerminalLog.packageEvents();
      }, terminalLogIntervalInSecs * 1000);
    }
  };

  OB.UTIL.stopTerminalLogSync = function() {
    if (OB.UTIL.terminalLogSyncIntervalId) {
      clearInterval(OB.UTIL.terminalLogSyncIntervalId);
      OB.UTIL.terminalLogSyncIntervalId = null;
    }
  };

  OB.UTIL.activateTerminalLogBackup = function() {
    if (!OB.UTIL.terminalLogBackupIntervalId) {
      var terminalLogBackupIntervalInSecs;
      if (OB.MobileApp.model.get('permissions')) {
        terminalLogBackupIntervalInSecs = OB.MobileApp.model.get('permissions')
          .OBMOBC_TerminalLogBackupInterval;
      }
      if (!terminalLogBackupIntervalInSecs) {
        // default value for OBMOBC_TerminalLogBackupInterval
        terminalLogBackupIntervalInSecs = 1;
      }
      OB.UTIL.terminalLogBackupIntervalId = setInterval(function() {
        OB.UTIL.TerminalLog.doBackup();
      }, terminalLogBackupIntervalInSecs * 1000);
    }
  };

  OB.UTIL.stopTerminalLogBackup = function() {
    if (OB.UTIL.terminalLogBackupIntervalId) {
      clearInterval(OB.UTIL.terminalLogBackupIntervalId);
      OB.UTIL.terminalLogBackupIntervalId = null;
    }
  };
})();
