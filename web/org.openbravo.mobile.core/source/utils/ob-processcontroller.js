/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 *
 * Author MDJ
 *
 */

/*global OB, Backbone, _ */

/*
  ProcessController manage processes of the application and notifies components subscribed to the processes
*/

OB.UTIL = OB.UTIL || {};

(function() {
  OB.UTIL.Process = Backbone.Model.extend({
    defaults: {
      name: null,
      searchkey: null,
      description: null,
      terminallog: null,
      subscriptions: [],
      //List of components subscribed to the process
      executions: new Backbone.Collection() //List of process instances in execution
    },
    initialize: function(attributes) {
      if (attributes) {
        this.set('name', attributes.name);
        this.set('searchkey', attributes.searchkey);
        this.set('description', attributes.description);
        this.set('terminallog', attributes.terminallog);
        this.set('subscriptions', []);
        this.set('executions', new Backbone.Collection());
      }
    }
  });
  OB.UTIL.ProcessController = {
    processes: new Backbone.Collection(),
    initialize: function() {
      var me = this;
      if (OB.UTIL.localStorage.getItem('processes')) {
        _.each(JSON.parse(OB.UTIL.localStorage.getItem('processes')), function(
          process
        ) {
          me.processes.push(new OB.UTIL.Process(process));
        });
      }
      setInterval(function() {
        _.each(OB.UTIL.ProcessController.getProcessesInExec().models, function(
          p
        ) {
          if (p.get('executions').models.length > 0) {
            _.each(p.get('executions').models, function(exec) {
              if (
                (Date.now() - exec.get('executionTime')) / 1000 >
                  OB.MobileApp.model.hasPermission(
                    'OBMOBC_ProcessesTimeOut',
                    true
                  ) &&
                !exec.get('timedOut')
              ) {
                exec.set('timedOut', true);
                OB.error(
                  '[ProcessController] Process ' +
                    p.get('name') +
                    ' with id ' +
                    exec.get('id') +
                    ' has reached ' +
                    OB.MobileApp.model.hasPermission(
                      'OBMOBC_ProcessesTimeOut',
                      true
                    ) +
                    ' seconds timeout'
                );
              }
            });
          }
        });
      }, 60000);
    },
    getProcessesInExec: function() {
      // Returns the processes in execution which the passed component is subscribed to
      return new Backbone.Collection(
        _.filter(OB.UTIL.ProcessController.processes.models, function(p) {
          var objInProcess = _.find(p.get('subscriptions'), function(o) {
            if (p.get('executions').models.length > 0) {
              return true;
            }
          });
          return objInProcess ? p : false;
        })
      );
    },
    getProcessesInExecLog: function() {
      var processes = OB.UTIL.ProcessController.getProcessesInExec();
      if (processes.length > 0) {
        var processesLog = '\n \n [Processes in execution]: \n';
        _.each(processes.models, function(p) {
          processesLog =
            processesLog + '\t- ' + p.get('name') + ': ' + p.get('description');
        });
        return processesLog;
      }
      return '';
    },
    getProcessesInExecByOBj: function(obj) {
      // Returns the processes in execution which the passed component is subscribed to
      return new Backbone.Collection(
        _.filter(OB.UTIL.ProcessController.processes.models, function(p) {
          var objInProcess = _.find(p.get('subscriptions'), function(o) {
            if (o === obj && p.get('executions').models.length > 0) {
              return true;
            }
          });
          return objInProcess ? p : false;
        })
      );
    },
    getProcessesBySearchkey: function(processesSearchKey) {
      //Load from localStorage if memory was cleared
      if (this.processes.length === 0) {
        this.initialize();
      }
      var processes = _.filter(this.processes.models, function(p) {
        return _.find(processesSearchKey, function(sk) {
          return p.get('searchkey') === sk;
        });
      });

      if (!processes || processes.length === 0) {
        OB.error(
          "[ProcessController] The process you are looking for doesn't exist. Check them: " +
            processesSearchKey
        );
      }
      return processes;
    },
    start: function(searchkey) {
      var me = this,
        uuid = OB.UTIL.get_UUID();
      var execProcess = this.getProcessesBySearchkey([searchkey])[0];
      var execution = new Backbone.Model({
        id: uuid,
        executionTime: Date.now(),
        timedOut: false
      });
      //add it to processes in execution list
      execProcess.get('executions').add(execution);
      _.each(execProcess.get('subscriptions'), function(obj) {
        if (obj.processStarted) {
          obj.processStarted(
            execProcess,
            execution,
            me.getProcessesInExecByOBj(obj)
          );
        }
      });
      if (execProcess.get('terminallog')) {
        OB.UTIL.TerminalLog.addProcess(searchkey, execution, 'start');
      }
      return execution;
    },
    finish: function(searchkey, execution) {
      var me = this,
        execProcess = this.getProcessesBySearchkey([searchkey])[0];
      //remove it from processes in execution list
      execProcess.get('executions').remove(execution);
      _.each(execProcess.get('subscriptions'), function(obj) {
        if (obj.processFinished) {
          obj.processFinished(
            execProcess,
            execution,
            me.getProcessesInExecByOBj(obj)
          );
        }
      });
      if (execProcess.get('terminallog')) {
        OB.UTIL.TerminalLog.addProcess(searchkey, execution, 'finish');
      }
    },
    subscribe: function(processesSearchKey, obj) {
      //Ignore components with no processes defined
      if (!processesSearchKey) {
        return;
      }
      var prcss = this.getProcessesBySearchkey(processesSearchKey);
      if (prcss.length !== processesSearchKey.length) {
        OB.error(
          '[ProcessController] You cannot subscribe the component ' +
            obj.name +
            ' to the process: ' +
            processesSearchKey +
            ' because it is not registered in Mobile Processes window in backend'
        );
      }
      _.each(prcss, function(prcs) {
        prcs.get('subscriptions').push(obj);
      });
    },
    unSubscribe: function(processesSearchKey, obj) {
      //Ignore components with no processes defined
      if (!processesSearchKey) {
        return;
      }
      var prcss = this.getProcessesBySearchkey(processesSearchKey);
      _.each(prcss, function(prcs) {
        var objInProcess = _.find(prcs.get('subscriptions'), function(o) {
          if (o === obj) {
            return true;
          }
        });
        if (!objInProcess) {
          OB.error(
            '[ProcessController] You cannot unsubscribe. The component ' +
              obj.name +
              ' is not subscribed to the process: ' +
              prcs.get('name')
          );
        }
        prcs
          .get('subscriptions')
          .splice(prcs.get('subscriptions').indexOf(objInProcess), 1);
      });
    }
  };
})();
