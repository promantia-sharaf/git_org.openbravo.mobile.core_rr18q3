/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _, moment */

/**
 * API to log user action and processes start and finish
 *
 */

(function() {
  OB.UTIL = window.OB.UTIL || {};
  OB.UTIL.TerminalLog = {
    events: [],
    eventsMaximumNumber: 10000,
    addKeyPress: function(elmt) {
      if (
        OB.MobileApp.model.get('permissions') &&
        !OB.MobileApp.model.hasPermission(
          'OBMOBC_EnableTerminalLogUserActions',
          true
        )
      ) {
        return;
      }

      if (this.events.length >= this.eventsMaximumNumber) {
        return;
      }

      var keyName = elmt.key;
      var password = false;
      if (
        elmt.dispatchTarget.type === 'password' &&
        keyName !== 'Enter' &&
        keyName !== 'Tab'
      ) {
        // don't save log of key pressed in input type password
        keyName = '';
        password = true;
      }
      var ids = elmt.srcElement.id.split('_');
      var previousEvent = this.events[this.events.length - 1];
      var context = this.getContext(ids);
      if (
        previousEvent &&
        previousEvent.t === 'k' &&
        previousEvent.c === context &&
        previousEvent.k.substr(previousEvent.k.length - 3) !== 'Tab'
      ) {
        if (!password && previousEvent.k.split(keyName).length <= 50) {
          this.events[this.events.length - 1].k += ', ' + keyName;
        }
      } else {
        var event = this.getNewEvent(context);
        event.t = 'k';
        if (password) {
          event.k = '[password] ' + keyName;
        } else {
          event.k = keyName;
          event.e = this.getElemFromIds(ids);
        }
        this.events.push(event);
      }
    },
    addButtonClick: function(elmt) {
      if (
        OB.MobileApp.model.get('permissions') &&
        !OB.MobileApp.model.hasPermission(
          'OBMOBC_EnableTerminalLogUserActions',
          true
        )
      ) {
        return;
      }
      var isKeyPadButton = function(txt, ids) {
        // keypadcontainer include all the numbers and '.' of keypad, and also all the coins. But it don't include others like enter, backspace or + , -, etc
        return txt.search(/[0-9.]+/) === 0 && ids['6'] === 'keypadcontainer';
      };
      if (this.events.length >= this.eventsMaximumNumber) {
        return;
      }
      var ids = elmt.id.split('_');
      var buttonName = this.getButtonName(elmt, ids);
      var previousEvent = this.events[this.events.length - 1];
      var context = this.getContext(ids);

      if (
        isKeyPadButton(buttonName, ids) &&
        previousEvent &&
        previousEvent.t === 'bk' &&
        previousEvent.c === context
      ) {
        previousEvent.b += ', ' + buttonName;
      } else {
        var event = this.getNewEvent(context);
        event.t = isKeyPadButton(buttonName, ids) ? 'bk' : 'b';
        event.b = buttonName;
        event.e = this.getElemFromIds(ids);
        this.events.push(event);
      }
    },
    getButtonName: function(elmt, ids) {
      var buttonName = '';
      if (elmt.tagName === 'INPUT') {
        buttonName += ' [input_field] ';
      }
      if (elmt.textContent) {
        // button label text
        buttonName += elmt.textContent;
      } else {
        // use the enyo object name
        buttonName += ids[ids.length - 1];
        if (buttonName === 'button') {
          // for keypad use the enyo parent
          buttonName += ids[ids.length - 2];
        }

        // make more clean the guessed name form enyo object (i case insensitive, g all occurrences)
        buttonName = buttonName.replace(/button/gi, '');
        buttonName = buttonName.replace(/btn/gi, '');
        buttonName = buttonName.replace(/entity/gi, '');
      }
      if (buttonName.length > 60) {
        buttonName = buttonName.substring(0, 60) + '[...]';
      }
      return buttonName;
    },
    getContext: function(ids) {
      var context = [];
      context.push(OB.MobileApp.model.get('terminalLogContext'));
      if (
        ids &&
        (!OB.MobileApp.model.get('terminalLogContextPopUp') ||
          OB.MobileApp.model.get('terminalLogContextPopUp').length === 0)
      ) {
        context.push(this.getContextFromIds(ids));
      } else {
        var terminalLogContextPopUp = OB.MobileApp.model.get(
          'terminalLogContextPopUp'
        );
        if (terminalLogContextPopUp && terminalLogContextPopUp.length > 0) {
          context = context.concat(terminalLogContextPopUp);
        }
      }
      var contextString = context.join('  |  ');
      if (contextString.length > 250) {
        contextString = contextString.substring(1, 245) + '[...]';
      }
      return contextString;
    },
    getContextFromIds: function(ids) {
      return ids[3] + ' - ' + ids[4];
    },
    getElemFromIds: function(ids) {
      return ids[ids.length - 2] + ' - ' + ids[ids.length - 1];
    },
    addProcess: function(name, id, status) {
      if (
        OB.MobileApp.model.get('permissions') &&
        !OB.MobileApp.model.hasPermission(
          'OBMOBC_EnableTerminalLogProcess',
          true
        )
      ) {
        return;
      }
      if (this.events.length >= this.eventsMaximumNumber) {
        return;
      }

      var event = this.getNewEvent();
      if (status === 'start') {
        event.t = 's';
        event.v = name;
        event.i = id;
      } else if (status === 'finish') {
        event.t = 'f';
        event.v = name;
        event.i = id;
      } else {
        OB.warn(
          "Called OB.UTIL.TerminalLog.addProcess with status '" +
            status +
            "', but this status is not one of accepted ones."
        );
      }
      this.events.push(event);
    },
    addPopup: function(popupName, status) {
      if (
        OB.MobileApp.model.get('permissions') &&
        !OB.MobileApp.model.hasPermission(
          'OBMOBC_EnableTerminalLogUserActions',
          true
        ) &&
        !OB.MobileApp.model.hasPermission(
          'OBMOBC_EnableTerminalLogProcess',
          true
        )
      ) {
        return;
      }
      var terminalLogContextPopUp = OB.MobileApp.model.get(
        'terminalLogContextPopUp'
      );
      var index = terminalLogContextPopUp.indexOf(popupName);
      if (status === 'open') {
        if (index < 0) {
          // show could be executed when the popup is still visible
          terminalLogContextPopUp.push(popupName);
        }
      } else if (status === 'close') {
        if (index >= 0) {
          terminalLogContextPopUp.splice(index, 1);
        }
      } else {
        OB.warn(
          "Called OB.UTIL.TerminalLog.addPopup with status '" +
            status +
            "', but this status is not one of accepted ones."
        );
      }
      OB.MobileApp.model.set(
        'terminalLogContextPopUp',
        terminalLogContextPopUp
      );
    },
    getNewEvent: function(context) {
      var event = {};
      event.d = new Date().getTime();
      event.o = OB.MobileApp.model.get('connectedToERP');
      event.c = context ? context : this.getContext();
      return event;
    },
    getEventsLog: function() {
      return this.events;
    },
    packageEvents: function(successCallback) {
      function callSuccessCallback() {
        if (successCallback) {
          successCallback();
        }
      }

      if (!OB.MobileApp.model) {
        // check if application is available
        return callSuccessCallback();
      }
      if (this.events.length === 0) {
        // if nothing to package, don't do the package :)
        return callSuccessCallback();
      }

      if (OB.MobileApp.model.get('sessionLost')) {
        // if we are not logged it, we continue saving user events but we don't package and sync them
        return callSuccessCallback();
      }

      // synchronous part
      var eventPackage = new OB.Model.TerminalLog();
      eventPackage.set(
        'terminal',
        OB.MobileApp.model.get('logConfiguration').deviceIdentifier
      );
      eventPackage.set(
        'cacheSessionId',
        OB.UTIL.localStorage.getItem('cacheSessionId')
      );
      eventPackage.set('events', JSON.stringify(this.events.slice())); // only clones first level, and events are immutable
      this.events = [];
      // asynchronous part
      // inside a transaction
      // delete backup
      // save model to sync
      OB.Dal.transaction(function(tx) {
        OB.UTIL.TerminalLog.deleteBackup();
        OB.Dal.saveInTransaction(
          tx,
          eventPackage,
          function() {
            callSuccessCallback();
          },
          function() {
            OB.error('Could not package events');
            callSuccessCallback();
          }
        );
      });
    },
    doBackup: function() {
      OB.UTIL.localStorage.setItem(
        'eventLogBackup',
        JSON.stringify(this.events)
      );
    },
    restoreFromBackup: function() {
      var eventsBackup = OB.UTIL.localStorage.getItem('eventLogBackup');
      if (eventsBackup && eventsBackup !== 'undefined') {
        this.events = JSON.parse(eventsBackup);
      }
    },
    deleteBackup: function() {
      OB.UTIL.localStorage.setItem('eventLogBackup', JSON.stringify([]));
    }
  };
})();
