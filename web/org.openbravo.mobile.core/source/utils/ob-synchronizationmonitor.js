/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 *
 * Author MDJ
 *
 */

/*global OB, Backbone, enyo */

OB.UTIL = OB.UTIL || {};

(function() {
  OB.UTIL.SynchronizationMonitor = {
    status: {
      SYNCHRONIZED: 0,
      SYNCHRONIZING: 1,
      NOTSYNCHRONIZED: 2,
      UNKNOWN: -1
    },
    currentStatus: -1,
    modelsSynchronized: function() {
      this.currentStatus = this.status.SYNCHRONIZED;
      OB.UTIL.showI18NSuccess(
        'OBMOBC_SynchronizationWasSuccessfulMessage',
        'OBMOBC_SynchronizingDataMessage'
      );
    },
    modelsSynchronizing: function() {
      this.currentStatus = this.status.SYNCHRONIZING;
      OB.UTIL.showI18NWarning(
        'OBMOBC_SynchronizingDataMessage',
        'OBMOBC_SynchronizationWasSuccessfulMessage'
      );
    },
    modelsNotSynchronized: function() {
      this.currentStatus = this.status.NOTSYNCHRONIZED;
    },
    isModelsSynchronized: function() {
      return this.currentStatus === this.status.SYNCHRONIZED;
    },
    isModelsSynchronizing: function() {
      return this.currentStatus === this.status.SYNCHRONIZING;
    },
    isModelsNotSynchronized: function() {
      return this.currentStatus === this.status.NOTSYNCHRONIZED;
    }
  };
})();
