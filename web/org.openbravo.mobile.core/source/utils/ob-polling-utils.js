/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, OB, _ */

OB.PollingUtils = {
  _getAssociatedSyncModel: function(message) {
    return OB.MobileApp.model.getSyncModelByModelName(message.get('modelName'));
  },
  getPollingUrl: function(message) {
    var modelFound;
    modelFound = OB.PollingUtils._getAssociatedSyncModel(message);
    if (
      modelFound &&
      modelFound.pollingClassName &&
      _.isString(modelFound.pollingClassName) &&
      modelFound.pollingClassName.length > 0
    ) {
      return modelFound.pollingClassName;
    }
    return null;
  },
  getPollingMaxAttempts: function(message) {
    var modelFound;
    modelFound = OB.PollingUtils._getAssociatedSyncModel(message);
    if (
      modelFound &&
      modelFound.maxAttempts &&
      _.isNumber(modelFound.maxAttempts) &&
      modelFound.maxAttempts > 0
    ) {
      return modelFound.maxAttempts;
    }
    return 10;
  },
  getPollingRetryWaitTime: function(message) {
    var modelFound;
    modelFound = OB.PollingUtils._getAssociatedSyncModel(message);
    if (
      modelFound &&
      modelFound.millisecondsBetweenPollingRequests &&
      _.isNumber(modelFound.millisecondsBetweenPollingRequests) &&
      modelFound.millisecondsBetweenPollingRequests > 300
    ) {
      return modelFound.millisecondsBetweenPollingRequests;
    }
    return 300;
  },
  createMessage: function(obj, url) {
    var message = new OB.Model.Message();
    message.set('service', OB.RR.RequestRouter.getServiceByName(url));
    message.set('modelName', obj.modelName);
    message.set('url', url);
    message.set('time', new Date().getTime()); //Take into account that this time is going to be different from the final processed message
    message.set('messageObj', obj.data[0]);
    return message;
  }
};
