/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, OB, console, _, Promise */
(function() {
  OB.Polling = window.OB.Polling || {};
  OB.Polling.PollingRequestHandler = {
    //FIXME: Move these lists to localStorage
    planedToSendPollingRequest: new OB.Collection.PollingRequestList(),
    notStartedPollingRequest: new OB.Collection.PollingRequestList(),
    ongoingPollingRequest: new OB.Collection.PollingRequestList(),
    successPollingRequest: new OB.Collection.PollingRequestList(),
    errorPollingRequest: new OB.Collection.PollingRequestList(),
    pollingSyncModels: undefined,
    pollingIsRunning: false,
    //Get Polling models and assign to them messages in notStartedPollingRequest list
    getPollingSyncModels: function() {
      var me = this,
        modelsFound = _.filter(
          OB.MobileApp.model.get('dataSyncModels'),
          function(curModel) {
            if (curModel.pollingClassName) {
              var messages = _.filter(
                me.notStartedPollingRequest.models,
                function(msg) {
                  return msg.get('syncModel') === curModel.name;
                }
              );
              //Move message from notStarted list to model list
              curModel.messagesByModel = new OB.Collection.PollingRequestList(
                messages
              );
              me.notStartedPollingRequest.remove(messages);
              return true;
            }
          }
        );
      if (modelsFound) {
        return modelsFound;
      }
      return null;
    },
    initializePolling: function() {
      this.pollingIsRunning = true;
      this.planedToSendPollingRequest = new OB.Collection.PollingRequestList();
      this.successPollingRequest = new OB.Collection.PollingRequestList();
      this.errorPollingRequest = new OB.Collection.PollingRequestList();
      this.pollingSyncModels = this.getPollingSyncModels();
      //When all models complete polling process sent finished trigger
      this.allMessagesSuccessfullyChecked = _.after(
        this.pollingSyncModels.length,
        this.raiseEvent_AllMessagesSuccessfullyChecked
      );
    },
    addPollingRequest: function(msg) {
      this.notStartedPollingRequest.add(
        new OB.Model.PollingRequest({
          message: msg
        })
      );
    },
    startPollingAction: function() {
      var me = this,
        process;
      OB.MobileApp.model.trigger('pollingStarted', {
        planedToSendPollingRequest: this.planedToSendPollingRequest,
        notStartedPollingRequest: this.notStartedPollingRequest,
        ongoingPollingRequest: this.ongoingPollingRequest
      });
      if (me.pollingIsRunning) {
        var msg = '[PollRequest] Polling is already running';
        OB.info(msg);
        OB.MobileApp.model.trigger('pollingFinished', {
          status: 'running',
          message: msg,
          successPollingRequest: this.successPollingRequest,
          errorPollingRequest: this.errorPollingRequest,
          notStartedPollingRequest: this.notStartedPollingRequest,
          ongoingPollingRequest: this.ongoingPollingRequest
        });
        return;
      }
      this.initializePolling();
      //We will do polling for models in parallel(asynchronous) and we finish when all are done
      _.each(me.pollingSyncModels, function(model) {
        me.pollingByModel(model);
      });
    },
    pollingByModel: function(model) {
      var me = this,
        process = new OB.DS.Process(model.pollingClassName);
      me.raiseEvent_PollingActionStarted(model);
      me.ongoingPollingRequest.add(model.messagesByModel.models);
      model.messagesByModel.increaseAttempts();
      process.exec(
        {
          messages: model.messagesByModel.serialize()
        },
        function(response) {
          me.finishPollingAction(
            response ? response.messages : null,
            response,
            model
          );
        },
        function(response) {
          me.finishPollingAction(
            response ? response.messages : null,
            response,
            model
          );
        }
      );
    },
    finishPollingAction: function(serializedMessages, response, model) {
      var me = this,
        promises = [],
        messages = model.messagesByModel.unserialize(serializedMessages);
      if (response && response.exception) {
        me.raiseEvent_UnexpectedModelError(model, response.exception.message);
        return;
      }
      if (!serializedMessages) {
        me.raiseEvent_UnexpectedModelError(
          model,
          'Polling Request did not return any messages'
        );
        return;
      }
      _.each(messages, function(msg) {
        promises.push(
          new Promise(function(resolve, reject) {
            if (msg.get('importStatus') === 'Processed') {
              me.successPollingRequest.add(msg);
              me.ongoingPollingRequest.remove(msg);
              model.messagesByModel.remove(msg);
            } else if (
              msg.get('importStatus') === 'Initial' &&
              msg.get('numberOfAttempts') > msg.get('maxAttempts')
            ) {
              me.raiseEvent_PollingActionFailed(msg);
              me.errorPollingRequest.add(msg);
              me.ongoingPollingRequest.remove(msg);
              model.messagesByModel.remove(msg);
            } else if (msg.get('importStatus') === 'Error') {
              me.errorPollingRequest.add(msg);
              me.ongoingPollingRequest.remove(msg);
              model.messagesByModel.remove(msg);
            }
            //if the message is in Initial status but didn't reach maxAttempts will remain in messagesByModel to retry polling
            resolve();
          })
        );
      });

      Promise.all(promises).then(function() {
        if (model.messagesByModel.length === 0) {
          me.raiseEvent_PollingActionModelSuccess(model);
          me.allMessagesSuccessfullyChecked();
        } else {
          me.retry(model);
        }
      });
    },
    retry: function(model) {
      var me = this;
      OB.info(
        '[PollRequest] Retriying Polling Request for model: ' +
          model.name +
          ' with ' +
          model.messagesByModel.length +
          ' messages'
      );
      setTimeout(function() {
        me.pollingByModel(model);
      }, model.millisecondsBetweenPollingRequests);
    },

    raiseEvent_PollingActionStarted: function(model) {
      OB.info(
        '[PollRequest] Polling for ' +
          model.name +
          ' is going to start with ' +
          model.messagesByModel.length +
          ' messages'
      );
    },
    raiseEvent_PollingActionModelSuccess: function(model) {
      OB.info(
        '[PollRequest] Polling for ' +
          model.name +
          ' finished with all messages confirmed.'
      );
    },
    raiseEvent_PollingActionFailed: function(pollingMsg) {
      OB.info(
        '[PollRequest] Polling for ' +
          pollingMsg.get('messageId') +
          ' has failed after ' +
          pollingMsg.get('numberOfAttempts') +
          ' attepmts'
      );
    },
    raiseEvent_AllMessagesSuccessfullyChecked: function() {
      this.pollingIsRunning = false;
      if (this.notStartedPollingRequest.length > 0) {
        this.startPollingAction();
      }
      OB.MobileApp.model.trigger('pollingFinished', {
        status: 'success',
        successPollingRequest: this.successPollingRequest,
        errorPollingRequest: this.errorPollingRequest
      });
      console.log('[PollRequest] Polling finished!');
    },
    raiseEvent_UnexpectedModelError: function(model, message) {
      this.raiseEvent_UnexpectedError(
        '[PollRequest] Polling for ' +
          model.name +
          ' finished with ERROR [' +
          message +
          '].'
      );
    },
    raiseEvent_UnexpectedError: function(message) {
      var strError = message || 'Unknown error';
      OB.info(strError);
      this.pollingIsRunning = false;
      OB.MobileApp.model.trigger('pollingFinished', {
        status: 'error',
        message: strError,
        successPollingRequest: this.successPollingRequest,
        errorPollingRequest: this.errorPollingRequest,
        notStartedPollingRequest: this.notStartedPollingRequest,
        ongoingPollingRequest: this.ongoingPollingRequest
      });
    },
    hasNotStartedPollingRequests: function() {
      return this.notStartedPollingRequest.length > 0;
    }
  };
})();
