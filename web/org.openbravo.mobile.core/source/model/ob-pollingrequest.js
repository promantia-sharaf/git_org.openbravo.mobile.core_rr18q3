/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, OB, _ */

OB.Model = OB.Model || {};
OB.Collection = OB.Collection || {};

OB.Model.PollingRequest = Backbone.Model.extend({
  defaults: {
    messageId: null,
    message: null,
    _messageData: null,
    syncModel: null,
    pollingUrl: null,
    numberOfAttempts: 0,
    maxAttempts: 10,
    pauseBetweenRetry: 300
  },
  initialize: function(obj) {
    if (!obj.message.get) {
      obj.message = new Backbone.Model(obj.message);
    }
    var msg =
      typeof obj.message.get('messageObj') === 'string'
        ? JSON.parse(obj.message.get('messageObj'))
        : obj.message.get('messageObj');
    this.set('_messageData', msg);
    this.set('messageId', msg.messageId);
    this.set('syncModel', obj.message.get('modelName'));
    this.set('pollingUrl', OB.PollingUtils.getPollingUrl(obj.message));
    this.set('maxAttempts', OB.PollingUtils.getPollingMaxAttempts(obj.message));
    this.set(
      'pauseBetweenRetry',
      OB.PollingUtils.getPollingRetryWaitTime(obj.message)
    );
    return true;
  },
  serialize: function() {
    var objToSend = {};
    objToSend.messageId = this.get('messageId');
    objToSend.syncModel = this.get('syncModel');
    objToSend.numberOfAttempts = this.get('numberOfAttempts');
    objToSend.elements = this.getElements();
    return objToSend;
  },
  unserialize: function(serializedModel) {
    var me = this;
    //set all new attributes to the model
    _.each(_.keys(serializedModel), function(key) {
      me.set(key, serializedModel[key]);
    });
  },
  getElements: function() {
    return this.get('_messageData').data || this.get('_messageData');
  },
  increaseAttempt: function() {
    this.set('numberOfAttempts', this.get('numberOfAttempts') + 1);
  }
});

OB.Collection.PollingRequestList = Backbone.Collection.extend({
  model: OB.Model.PollingRequest,
  serialize: function() {
    var arrayToSend = [];
    _.each(this.models, function(model) {
      arrayToSend.push(model.serialize());
    });
    return arrayToSend;
  },
  unserialize: function(serializedMessages) {
    var me = this,
      arrayToSend = [];
    _.each(serializedMessages, function(serializedModel) {
      arrayToSend.push(
        _.find(me.models, function(model) {
          if (model.get('messageId') === serializedModel.messageId) {
            model.unserialize(serializedModel);
            return model;
          }
        })
      );
    });
    return arrayToSend;
  },
  getElements: function() {
    var me = this,
      arrayToSend = [];
    _.each(this.models, function(model) {
      arrayToSend = arrayToSend.concat(model.getElements());
    });
    return arrayToSend;
  },
  increaseAttempts: function() {
    _.each(this.models, function(model) {
      model.increaseAttempt();
    });
  }
});
