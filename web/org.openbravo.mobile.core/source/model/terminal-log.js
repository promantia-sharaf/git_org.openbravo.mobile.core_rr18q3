/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone */

(function() {
  var TerminalLog = OB.Data.ExtensibleModel.extend({
    modelName: 'TerminalLog',
    tableName: 'OBMOBC_TerminalLog',
    entityName: 'OBMOBC_TerminalLog',
    source: '',
    local: true
  });

  TerminalLog.addProperties([
    {
      name: 'id',
      column: 'terminallog_id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'terminal',
      column: 'terminal',
      type: 'TEXT'
    },
    {
      name: 'cacheSessionId',
      column: 'cacheSessionId',
      type: 'TEXT'
    },
    {
      name: 'events',
      column: 'events',
      type: 'TEXT'
    }
  ]);

  OB.Data.Registry.registerModel(TerminalLog);
})();
